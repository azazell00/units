class State(object):
    def __init__(self, hit_points, magic_vulnerability = 1, physical_vulnerability = 1):
        self.observers = set()
        self.max_hit_points = hit_points
        self.hit_points = hit_points
        self.is_undead = False
        self.magic_vulnerability = magic_vulnerability
        self.physical_vulnerability = physical_vulnerability

    def get_damage(self, damage, is_magic):
        if is_magic:
            damage = int(damage * self.magic_vulnerability)
        else:
            damage = int(damage * self.physical_vulnerability)

        new_hit_points = self.hit_points - damage
        
        if damage < 0:
            raise ValueError
        
        if new_hit_points > 0:
            self.hit_points = new_hit_points
        else:
            self.hit_points = 0
            for observer in self.observers:
                observer.behold_death(self)

    def magic_vulnerability_effect(self, value):
        self.magic_vulnerability *= value

    def physical_vulnerability_effect(self, value):
        self.physical_vulnerability *= value

    def get_hit_points(self, hit_points):
        new_hit_points = self.hit_points + hit_points
        
        if hit_points < 0:
            raise ValueError

        if new_hit_points > self.max_hit_points:
            self.hit_points = self.max_hit_points
        else:
            self.hit_points = new_hit_points

    def add_observer(self, observer):
        self.observers.add(observer)
