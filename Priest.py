from .Healer import Healer
from .SpellNatures import SpellNatures


class Priest(Healer):
    def attack(self, aim):
        if aim.state.is_undead:
            self.basic_ability.action(self, aim, self.attack_power * 2)
        else:
            self.basic_ability.action(self, aim, self.attack_power)
        aim.contr_attack(self)

    def contr_attack(self, aim):
        if aim.state.is_undead:
            self.basic_ability.action(self, aim, self.attack_power)
        else:
            self.basic_ability.action(self, aim, self.attack_power // 2)
