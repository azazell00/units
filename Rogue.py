from .Unit import Unit


class Rogue(Unit):
    def attack(self, aim):
        self.basic_ability.action(self, aim, self.attack_power)
