from .Unit import Unit


class Berserker(Unit):
    def __init__(self, hit_points, attack_power):
        super(Berserker, self).__init__(hit_points, attack_power, magic_vulnerability = 0)
