from .SpellCaster import SpellCaster
from .SpellNatures import SpellNatures
from .abilities.DeathBreath import DeathBreath


class Necromancer(SpellCaster):
    specialisation = SpellNatures.necromancy

    def __init__(self, hit_points, attack_power,
                 magic_points, magick_power):

        super(Necromancer, self).__init__(hit_points, attack_power,
                                     magic_points, magick_power)
        self.active_spell = DeathBreath()
        self.add_spell(self.active_spell)

    def behold_death(self, observable_state):
        if self.state.hit_points != 0:
            self.state.get_hit_points(observable_state.max_hit_points // 6 + self.magic_power)
