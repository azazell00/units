from .State import State
from .MyExceptions import *

class SpellCasterState(State):
    def __init__(self, hit_points, magic_points,
                 magic_vulnerability = 1, physical_vulnerability = 1):
        super(SpellCasterState, self).__init__(
                                              hit_points,
                                              magic_vulnerability,
                                              physical_vulnerability
                                              )
        self.magic_points = magic_points

    def spend_magic_points(self, magic_points):
        new_magic_points = self.magic_points - magic_points
        
        if magic_points < 0:
            raise ValueError
        if new_magic_points < 0:
            raise NotEnoughManaError

        self.magic_points = new_magic_points
