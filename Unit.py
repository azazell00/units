from .State import State
from .MyExceptions import *
from .abilities.PhisycalAttack import PhisycalAttack


class Unit(object):

    def __init__(self, hit_points, attack_power, magic_vulnerability = 1,
                 physical_vulnerability = 1):
        self.attack_power = attack_power
        self.state = State(hit_points, magic_vulnerability, physical_vulnerability)
        self.basic_ability = PhisycalAttack()
        self.additional_abilities = dict()
        self.active_ability = None

    def attack(self, aim):
        self.basic_ability.action(self, aim, self.attack_power)
        aim.contr_attack(self)

    def contr_attack(self, aim):
        self.basic_ability.action(self, aim, self.attack_power // 2)
        
    def use_ability(self, aim):
        if self.active_ability == None:
            raise NoSpecialAbilities
            
        self.active_ability.action(self, aim)

    def add_ability(self, ability):
        if ability not in self.additional_abilities.values():
            self.additional_abilities[ability.name] = ability

    def set_active_ability(self, ability):
        if not ability in self.additional_abilities.values():
            raise UnfamiliarAbilityError

        self.active_ability = self.additional_abilities[ability.name]
