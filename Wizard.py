from .SpellCaster import SpellCaster
from .SpellNatures import SpellNatures
from .abilities.FireBall import FireBall


class Wizard(SpellCaster):
    specialisation = SpellNatures.element
    
    def __init__(self, hit_points, attack_power,
                 magic_points, magick_power):

        super(Wizard, self).__init__(hit_points, attack_power,
                                     magic_points, magick_power)
        self.active_spell = FireBall()
        self.add_spell(self.active_spell)

    
