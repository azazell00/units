from .SpellCaster import SpellCaster
from .SpellNatures import SpellNatures
from .abilities.Heal import Heal

class Healer(SpellCaster):
    specialisation = SpellNatures.light
    
    def __init__(self, hit_points, attack_power,
                 magic_points, magick_power,):

        super(Healer, self).__init__(hit_points, attack_power,
                                     magic_points, magick_power)
        self.active_spell = Heal()
        self.add_spell(self.active_spell)
