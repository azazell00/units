from enum import Enum


class SpellPurposes(Enum):
    combat = 1
    support = 2
    invoke = 3
