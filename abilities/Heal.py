from .Ability import Ability
from ..SpellPurposes import SpellPurposes
from ..SpellNatures import SpellNatures

class Heal(Ability):
    name = "Heal"
    nature = SpellNatures.light
    purpose = SpellPurposes.support

    def action(self, caster, aim, spell_power):
        if aim.state.hit_points == 0 or caster.state.hit_points == 0:
            raise DeadUnitError

        caster.state.spend_magic_points(spell_power)
        aim.state.get_hit_points(spell_power)
