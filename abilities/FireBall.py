from .CombatSpell import CombatSpell
from ..SpellNatures import SpellNatures


class FireBall(CombatSpell):
    name = "FireBall"
    nature = SpellNatures.element

    def action(self, caster, aim, spell_power):
        super(FireBall, self).action(caster, aim, spell_power)
