from abc import ABCMeta, abstractmethod
from .Ability import Ability
from ..SpellPurposes import SpellPurposes

class CombatSpell(Ability):
    __metaclass__ = ABCMeta

    purpose = SpellPurposes.combat

    @abstractmethod
    def action(self, caster, aim, spell_power):
        if aim.state.hit_points == 0 or caster.state.hit_points == 0:
            raise DeadUnitError
        if caster == aim:
            raise IncompatibleAimError

        caster.state.spend_magic_points(spell_power)
        aim.state.get_damage(spell_power, True)
