from .Ability import Ability
from .Transform import Transform


class WerwolfBite(Ability):
    name = "Werwolf Bite"

    def action(self, hitter, aim, attack_power):
        super(WerwolfBite, self).action(hitter, aim, attack_power)
        if not aim.basic_ability.name == "Vampirism":
            aim.basic_ability = self
            aim.add_ability(Transform())
