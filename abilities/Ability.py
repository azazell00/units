from abc import ABCMeta, abstractmethod
from ..MyExceptions import *


class Ability(object):
    __metaclass__ = ABCMeta

    instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(Ability, cls).__new__(
                                cls, *args, **kwargs)

        return cls.instance

    @abstractmethod
    def action(self, hitter, aim, attack_power):
        if aim.state.hit_points == 0 or hitter.state.hit_points == 0:
            raise DeadUnitError
        if aim == hitter:
            raise IncompatibleAimError

        aim.state.get_damage(attack_power, False)
