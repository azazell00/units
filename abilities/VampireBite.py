from .Ability import Ability


class VampireBite(Ability):
    name = "Vampirism"

    def action(self, hitter, aim, attack_power):
        aim_initial_hp = aim.state.hit_points

        super(VampireBite, self).action(hitter, aim, attack_power)
        hitter.state.get_hit_points((aim_initial_hp - aim.state.hit_points) // 4 + 1)
        if aim.basic_ability.name != "Werwolf Bite":
            aim.basic_ability = self
            aim.state.is_undead = True
