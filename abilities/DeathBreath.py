from .CombatSpell import CombatSpell
from ..SpellNatures import SpellNatures


class DeathBreath(CombatSpell):
    name = "Death Breath"
    nature = SpellNatures.necromancy

    def action(self, caster, aim, spell_power):
        super(DeathBreath, self).action(caster, aim, spell_power)

        caster.state.spend_magic_points(spell_power)
        aim.state.add_observer(caster)
