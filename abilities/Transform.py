from .Ability import Ability
from ..MyExceptions import *


class Transform(Ability):
    name = "Transform"

    def action(self, initiator, aim):
        if aim != initiator:
            raise IncompatibleAimError
        if initiator == 0:
            raise DeadUnitError

        if "is_transformed" not in aim.__dict__.keys():
            aim.is_transformed = False
        if not aim.is_transformed:
            aim.state.hit_points *= 2
            aim.state.max_hit_points *= 2
            aim.state.magic_vulnerability *= 2
            aim.attack_power *= 2
            aim.is_transformed = True
        else:
            aim.state.hit_points /= 2
            aim.state.max_hit_points /= 2
            aim.state.magic_vulnerability /= 2
            aim.attack_power /= 2
            aim.is_transformed = False
