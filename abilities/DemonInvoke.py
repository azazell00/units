from .Ability import Ability
from ..Soldier import Soldier
from ..SpellPurposes import SpellPurposes
from ..SpellNatures import SpellNatures


class DemonInvoke(Ability):
    name = "Demon Invoke"
    nature = SpellNatures.hell
    purpose = SpellPurposes.invoke

    def action(self, caster, aim, spell_power):
        if aim.state.hit_points == 0 or caster.state.hit_points == 0:
            raise DeadUnitError
        
        caster.state.spend_magic_points(spell_power)
        caster.demon = Soldier(aim.state.hit_points / 4 + spell_power,
                               aim.attack_power / 4 + spell_power / 4)
