from enum import Enum


class SpellNatures(Enum):
    element = 1
    light = 2
    hell = 3
    necromancy = 4
