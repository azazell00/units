from .SpellCaster import SpellCaster
from .SpellNatures import SpellNatures
from .abilities.DemonInvoke import DemonInvoke


class Warlock(SpellCaster):
    specialisation = SpellNatures.hell

    def __init__(self, hit_points, attack_power,
                 magic_points, magick_power):

        super(Warlock, self).__init__(hit_points, attack_power,
                                     magic_points, magick_power)
        self.active_spell = DemonInvoke()
        self.add_spell(self.active_spell)
        self.demon = None


