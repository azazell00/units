from .Unit import Unit
from .abilities.VampireBite import VampireBite


class Vampire(Unit):
    def __init__(self, hit_points, attack_power):
        super(Vampire, self).__init__(hit_points, attack_power)
        
        self.basic_ability = VampireBite()
        self.state.is_undead = True
