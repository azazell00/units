from .Unit import Unit
from .abilities.PhisycalAttack import PhisycalAttack
from .SpellCasterState import SpellCasterState
from .MyExceptions import *


class SpellCaster(Unit):
    def __init__(self, hit_points, attack_power,
                 magic_points, magic_power,
                 magic_vulnerability = 1, physical_vulnerability = 1):
        self.attack_power = attack_power
        self.magic_power = magic_power
        self.state = SpellCasterState(hit_points, magic_points, magic_vulnerability, physical_vulnerability)
        self.basic_ability = PhisycalAttack()
        self.additional_abilities = dict()
        self.active_ability = None
        self.spell_book = dict()
        self.active_spell = None

    def use_spell(self, aim):
        if self.specialisation == self.active_spell.nature:
            spell_power = self.magic_power
        else:
            spell_power = self.magic_power // 2
            
        self.active_spell.action(self, aim, spell_power)

    def set_active_spell(self, spell):
        if spell in self.spell.values():
            self.active_spell = self.spell_book[spell.name]
        else:
            raise UnfamiliarSpellError

    def add_spell(self, spell):
        if spell not in self.spell_book.values():
            self.spell_book[spell.name] = spell
