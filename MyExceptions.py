class DeadUnitError(Exception): 
    pass

class IncompatibleAimError(Exception):
    pass

class NotEnoughManaError(Exception):
    pass

class UnfamiliarSpellError(Exception):
    pass

class UnfamiliarAbilityError(Exception):
    pass

class NoSpecialAbilities(Exception):
    pass
