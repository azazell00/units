from .Unit import Unit
from .abilities.WerwolfBite import WerwolfBite
from .abilities.Transform import Transform


class Werwolf(Unit):
    def __init__(self, hit_points, attack_power):
        super(Werwolf, self).__init__(hit_points, attack_power)
        
        self.basic_ability = WerwolfBite()
        self.active_ability = Transform()
        self.additional_abilities = { "Transform": self.active_ability, }
